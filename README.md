# instructions on how to install the bby application.

This application aims to build a dataset of products from bby using its API.
To install this application the following things are needed.

Java Sdk
Mysql.
Glassfish Application Server
Maven 3.04 

1 ] 

Please follow the link for the installation manual of each tool as listed below
    a,http://docs.oracle.com/javase/7/docs/webnotes/install/
    b,https://dev.mysql.com/doc/refman/5.5/en/installing.html
    c,http://docs.oracle.com/cd/E26576_01/doc.312/e24935/installing.htm
    d,http://maven.apache.org/download.cgi.
    
2] 

Once the environment is set navigate into the rootfolder of the project from the command prompt and issue the command
mvn clean install .

3]
Launch the glassfish application server then create a database pool (bbypool) and a jndi name of jdbc/bbytest .


4]
 deploy the war file created by maven command in step 2 to the application server.
 

5]
 Go to the url and type http://127.0.0.1/bby


6]
Updates to the DB will be done every 24 hrs (It is a scheduled Job).
