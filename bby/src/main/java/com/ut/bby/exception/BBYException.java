package com.ut.bby.exception;

public class BBYException extends Exception{
    private static final long serialVersionUID = 1L;

    public BBYException(String s) {
        super(s);
    }

    public BBYException(Exception e) {
        super(e);
    }

    public BBYException(String s, Exception e) {
        super(s,e);
    }
}
