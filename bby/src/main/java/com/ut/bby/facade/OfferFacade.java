package com.ut.bby.facade;




import com.ut.bby.dbmodel.Offer;
import com.ut.bby.util.qualifier.PersistenceQualifier;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless
public class OfferFacade extends AbstractFacade<Offer> {

    @Inject
    @PersistenceQualifier
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public OfferFacade() {
        super(Offer.class);
    }

}