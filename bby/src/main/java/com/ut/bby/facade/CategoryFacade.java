package com.ut.bby.facade;



import com.ut.bby.dbmodel.Category;
import com.ut.bby.util.qualifier.PersistenceQualifier;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;


@Stateless
public class CategoryFacade extends AbstractFacade<Category> {

    @Inject
    @PersistenceQualifier
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }

}