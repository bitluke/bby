package com.ut.bby.facade;

import com.ut.bby.dbmodel.Category;
import com.ut.bby.dbmodel.Offer;
import com.ut.bby.dbmodel.Product;
import com.ut.bby.util.qualifier.PersistenceQualifier;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateless
public class ProductFacade extends AbstractFacade<Product> {
    @EJB
    private CategoryFacade categoryFacade;
    @EJB
    private OfferFacade offerFacade;

    @Inject
    @PersistenceQualifier
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }


    public void batchProductCreate(List<Product> entity) {
        int i = 0;
        for (Product en : entity) {
            //before persisting query by naturalId
            Product existingEntity = (Product) getSession().byNaturalId(Product.class).using("sku", en.getSku()).load();
            if (existingEntity != null) {
                updateOffers(en.getOfferList());
                updateCategories(en.getCategoryList());
                en.setId(existingEntity.getId());
                edit(en);
            } else {
                updateOffers(en.getOfferList());
                updateCategories(en.getCategoryList());
                create(en);
            }

             //do flushing and clearing at a constant rate to free memory ;)
            if (i % 40 == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }
            ++i;
        }

    }

    public void naturalProductUpdate(Product entity) {
            //before persisting query by naturalId
            Product existingEntity = (Product) getSession().byNaturalId(Product.class).using("sku", entity.getSku()).load();
            if (existingEntity != null) {
                updateOffers(entity.getOfferList());
                updateCategories(entity.getCategoryList());
                entity.setId(existingEntity.getId());
                edit(entity);
            }
    }

    private void updateOffers(List<Offer> offerList) {
        if (offerList != null) {
            for (Offer c : offerList) {
                if (c.getOfferId() != null || !c.getOfferId().isEmpty()) {
                    Offer existingCat = (Offer) getSession().byNaturalId(Offer.class).using("offerId", c.getOfferId()).load();
                    if (existingCat != null) {
                        c.setId(existingCat.getId());
                        offerFacade.edit(c);
                    } else {
                        offerFacade.create(c);
                    }
                }
            }
        }
    }

    private void updateCategories(List<Category> categories) {
        if (categories != null) {
            for (Category c : categories) {
                if (c.getCategoryId() != null || !c.getCategoryId().isEmpty()) {
                    Category existingCat = (Category) getSession().byNaturalId(Category.class).using("categoryId", c.getCategoryId()).load();
                    if (existingCat != null) {
                        c.setId(existingCat.getId());
                        categoryFacade.edit(c);
                    } else {
                        categoryFacade.create(c);
                    }
                }
            }
        }

    }

}