package com.ut.bby.facade;

import com.ut.bby.dbmodel.Review;
import com.ut.bby.util.qualifier.PersistenceQualifier;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateless
public class ReviewFacade extends AbstractFacade<Review> {

    @Inject
    @PersistenceQualifier
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public ReviewFacade() {
        super(Review.class);
    }

    public void batchReviewCreate(List<Review> reviews) {
        int i = 0;
        for (Review en : reviews) {
            //before persisting query by naturalId
            Review existingEntity = (Review) getSession().byNaturalId(Review.class).using("reviewId", en.getReviewId()).load();
            if (existingEntity != null) {
                edit(en);
            } else {
                create(en);
            }

            //do flushing and clearing at a constant rate to free memory ;)
            if (i % 40 == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }
            ++i;
        }

    }

    public void naturalReviewUpdate(Review entity) {
        //before persisting query by naturalId
        Review existingEntity = (Review) getSession().byNaturalId(Review.class).using("reviewId", entity.getReviewId()).load();
        if (existingEntity != null) {
            entity.setId(existingEntity.getId());
            edit(entity);
        }
    }
}