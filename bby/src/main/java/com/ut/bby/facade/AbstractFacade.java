package com.ut.bby.facade;



import org.hibernate.Session;
import org.jboss.solder.logging.Logger;
import org.jboss.solder.logging.Logger.Level;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractFacade<T> {
    private static final String JNDI_NAME = "jdbc/bby";

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void batchCreate(List<T> entity) {
        int i = 0;
        for (T en : entity) {
            getEntityManager().persist(en);
            if (i % 40 == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }
            ++i;
        }

    }


    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }


    public static Connection getConnection() {
        Context ctx = null;
        DataSource ds = null;
        Connection conn = null;
        try {
            ctx = new InitialContext();
            ds = (DataSource) ctx.lookup(JNDI_NAME);
            conn = ds.getConnection("root", "mysql");
        } catch (NamingException ex) {
            Logger.getLogger(AbstractFacade.class.getName()).log(Level.FATAL, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(AbstractFacade.class.getName()).log(Level.FATAL, null, ex);
        }
        return conn;
    }

    public static DataSource getDataSource() {
        Context ctx = null;
        DataSource ds = null;
        try {
            ctx = new InitialContext();
            ds = (DataSource) ctx.lookup(JNDI_NAME);

        } catch (NamingException ex) {
            Logger.getLogger(AbstractFacade.class.getName()).log(Level.FATAL, null, ex);
        }
        return ds;
    }


    public Session getSession() {
        return getEntityManager().unwrap(Session.class);
    }
}