package com.ut.bby.dbmodel;

import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
public class Review extends BaseModel {

    private String aboutMeCustomerType;  //aboutMe.customerType
    private String comment;
    private Long reviewId; //id
    private Float rating;
    private String reviewerName;//reviewer.name
    private Long sku;//foreign ;
    private Date submissionTime;
    private String title;

    public String getAboutMeCustomerType() {
        return aboutMeCustomerType;
    }

    public void setAboutMeCustomerType(String aboutMeCustomerType) {
        this.aboutMeCustomerType = aboutMeCustomerType;
    }

    @Column(length = 999999999)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @NaturalId
    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public Long getSku() {
        return sku;
    }

    public void setSku(Long sku) {
        this.sku = sku;
    }

    public Date getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Date submissionTime) {
        this.submissionTime = submissionTime;
    }

    @Column(length = 1000)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        if (reviewId != null ? !reviewId.equals(review.reviewId) : review.reviewId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return reviewId != null ? reviewId.hashCode() : 0;
    }
}
