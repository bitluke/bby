package com.ut.bby.action;

import com.ut.bby.dbmodel.Product;
import com.ut.bby.dbmodel.Review;
import com.ut.bby.facade.ProductFacade;
import com.ut.bby.facade.ReviewFacade;
import com.ut.bby.util.displaytag.PaginatedProductList;

import javax.ejb.EJB;
import java.util.ArrayList;
import java.util.List;


public class ProductAction extends BaseAction {

    @EJB
    private ProductFacade productFacade;
    @EJB
    private ReviewFacade reviewFacade;
    private List<Product> productList = new ArrayList<Product>();
    private List<Review> reviewList = new ArrayList<Review>();
    private PaginatedProductList<Product> paginatedProductList;
    private Long sku;


    public String retrieveProducts() throws Exception {
        List<Product> tempProducts = productFacade.getEntityManager().createQuery("From Product p where p.active = true order by p.sku asc").getResultList();
        if(tempProducts != null && !tempProducts.isEmpty()){
            productList = tempProducts.subList(0,100);
        }
        return SUCCESS;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String retrieveReviews() throws Exception {
        reviewList = reviewFacade.getEntityManager().createQuery("from Review r where r.sku =:skuid ").setParameter("skuid", sku).getResultList();
        return ! reviewList.isEmpty() ? SUCCESS : INPUT;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    public Long getSku() {
        return sku;
    }

    public void setSku(Long sku) {
        this.sku = sku;
    }
}
