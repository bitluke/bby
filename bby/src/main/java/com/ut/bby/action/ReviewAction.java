package com.ut.bby.action;

import com.ut.bby.dbmodel.Review;
import com.ut.bby.facade.ReviewFacade;

import javax.ejb.EJB;
import java.util.List;

public class ReviewAction extends BaseAction {
    private Review review;
    private List<Review> reviewList;

    @EJB
    private ReviewFacade reviewFacade;

    public String retrieveReviews() throws Exception {
        List<Review> tempReviews = reviewFacade.getEntityManager().createQuery("From Review r order by r.reviewId asc").getResultList();
        if(tempReviews != null && !tempReviews.isEmpty()){
            reviewList = tempReviews.subList(0,100);
        }
        return SUCCESS;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }
}
