package com.ut.bby.action;


import com.ut.bby.dbmodel.Review;
import com.ut.bby.exception.BBYException;
import com.ut.bby.facade.CategoryFacade;
import com.ut.bby.facade.ProductFacade;
import com.ut.bby.facade.ReviewFacade;
import com.ut.bby.util.xml.Converter;
import com.ut.bby.xmlmodel.Product;
import com.ut.bby.xmlmodel.Products;
import com.ut.bby.xmlmodel.Reviews;
import org.jboss.solder.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UploadAction extends BaseAction {


    @Inject
    Logger log;
    private File zipped;
    private String zippedFileName;
    private String zippedContentType;
    @EJB
    private ProductFacade productFacade;
    @EJB
    private CategoryFacade categoryFacade;
    private List<com.ut.bby.dbmodel.Product> productsToSend;
    private List<Review> reviewsTosend;
    @EJB
    private ReviewFacade reviewFacade;
    private boolean isProduct;

    public File getZipped() {
        return zipped;
    }

    public void setZipped(File zipped) {
        this.zipped = zipped;
    }

    public String getZippedFileName() {
        return zippedFileName;
    }

    public void setZippedFileName(String zippedFileName) {
        this.zippedFileName = zippedFileName;
    }

    public String getZippedContentType() {
        return zippedContentType;
    }

    public void setZippedContentType(String zippedContentType) {
        this.zippedContentType = zippedContentType;
    }


    @TransactionAttribute(value = TransactionAttributeType.MANDATORY)
    public String initDb() throws Exception {
        System.out.println("Start Time: " + new Date());
        ServletContext servletContext = getServletContext();
        String dataDir = servletContext.getRealPath("/WEB-INF");
        File savedFile = new File(dataDir, zippedFileName);
        zipped.renameTo(savedFile);
        ZipFile zipFile = new ZipFile(savedFile);
        Enumeration entriesEnum = zipFile.entries();

        /*Products ps = getBBYClient().getProducts();
        if(ps == null){
            return INPUT;
        }*/

        /*int i = 0;
        for(Product p : ps.getProducts()){


            productFacade.create(convertXmlProductToDbProduct(p));
//                    if ( i % 20 == 0 ) { //20, same as the JDBC batch size
//                        //flush a batch of inserts and release memory:
//                        productFacade.getEntityManager().flush();
//                        productFacade.getEntityManager().clear();
//                        productFacade.getEntityManager().getTransaction().begin();
//                    }
//                    i++;
        }*/

        while (entriesEnum.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entriesEnum.nextElement();
            if (entry.isDirectory()) {
                /*  *
                * Currently not unzipping the directory structure.
                * All the files will be unzipped in a Directory
                *
                **/
            } else {
                int index = 0;
                String name = entry.getName();
                index = entry.getName().lastIndexOf("/");
                if (index > 0 && index != name.length())
                    name = entry.getName().substring(index + 1);

                System.out.println(name);

                Products ps = getBBYClient().getProductsFromZipContent(zipFile.getInputStream(entry));
                // Products ps = getBBYClient().getProducts();

                int i = 0;
                productsToSend = new ArrayList<com.ut.bby.dbmodel.Product>();
                for (Product p : ps.getProducts()) {
                    // Session session = productFacade.getSession();
                    productsToSend.add(Converter.convertXmlProductToDbProduct(p));
                    //productFacade.getEntityManager();
                    /* if ( i % 20 == 0 ) { //20, same as the JDBC batch size
                        //flush a batch of inserts and release memory:
                        productFacade.getEntityManager().flush();
                        productFacade.getEntityManager().clear();
                        //productFacade.getEntityManager().getTransaction().begin();
                    }
                    i++;*/
                }
                productFacade.batchProductCreate(productsToSend);
            }
        }
        return SUCCESS;
    }


    public String uploadFileToDb() throws Exception {
        System.out.println("Start Time: " + new Date());
        ServletContext servletContext = getServletContext();
        String dataDir = servletContext.getRealPath("/WEB-INF");
        File savedFile = new File(dataDir, zippedFileName);
        zipped.renameTo(savedFile);
        ZipFile zipFile = new ZipFile(savedFile);
        Enumeration entriesEnum = zipFile.entries();


        while (entriesEnum.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entriesEnum.nextElement();
            if (entry.isDirectory()) {
                //process directories here , not important for us.
            } else {
                int index = 0;
                String name = entry.getName();
                index = entry.getName().lastIndexOf("/");
                if (index > 0 && index != name.length()) {
                    name = entry.getName().substring(index + 1);
                }
                System.out.println(name);


                //automatic sensing.
                byte[] littleOfThecontent = new byte[1024];
                InputStream ins = zipFile.getInputStream(entry);
                ins.read(littleOfThecontent);
                String s = new String(littleOfThecontent);
                isProduct = s.toLowerCase().contains("<products");

                if (s.toLowerCase().contains("<products")) {
                    Products ps = getBBYClient().getProductsFromZipContent(zipFile.getInputStream(entry));
                    if (ps != null) {
                        productsToSend = new ArrayList<com.ut.bby.dbmodel.Product>();
                        for (Product p : ps.getProducts()) {
                            productsToSend.add(Converter.convertXmlProductToDbProduct(p));
                        }
                        productFacade.batchProductCreate(productsToSend);
                    } else {
                        return INPUT;
                    }


                } else if (s.toLowerCase().contains("<reviews")) {
                    Reviews rs = getBBYClient().getReviewsFromZipContent(zipFile.getInputStream(entry));
                    //check the content of rs
                    if (rs != null) {
                        reviewsTosend = new ArrayList<Review>();
                        for (com.ut.bby.xmlmodel.Review r : rs.getReviewList()) {
                            reviewsTosend.add(Converter.convertXmlReviewToDbReview(r));
                        }
                        reviewFacade.batchReviewCreate(reviewsTosend);
                    } else {
                        return INPUT;
                    }


                }


            }
        }
//        if (isProduct) {
//            scheduleProductJobs();
//        } else {
//            scheduleReviewJobs();
//        }
        return SUCCESS;
    }

    public boolean isProduct() {
        return isProduct;
    }

    public void setProduct(boolean product) {
        isProduct = product;
    }

    public String upload() throws Exception {

        return SUCCESS;
    }


    private void scheduleReviewJobs() {
        if (!getSession().containsKey("reviewScheduler")) {
            Timer t = new Timer();
            TimerTask tt = new TimerTask() {
                @Override
                public void run() {
                    try {

                        log.info("executing Review job");

                        List<BigInteger> reviewIdList = reviewFacade.getSession().getSessionFactory().openSession().createSQLQuery("select reviewId from review").list();
                        for (BigInteger id : reviewIdList) {
                            com.ut.bby.xmlmodel.Review xmlReview = client.getReview(id.intValue());
                            if (xmlReview != null) {
                                reviewFacade.naturalReviewUpdate(Converter.convertXmlReviewToDbReview(xmlReview));
                            }
                        }
                    } catch (BBYException e) {
                        log.error("", e);
                    }
                }
            };
            //t.schedule(tt, Calendar.getInstance().getTime(), 24 * 60 * 60 * 1000);
            t.schedule(tt, Calendar.getInstance().getTime(), 30 * 1000);
            getSession().put("reviewScheduler", 1);
            System.out.println("done adding Review job");
        }
    }

    private void scheduleProductJobs() {
        if (!getSession().containsKey("productScheduler")) {

        }


    }


}
