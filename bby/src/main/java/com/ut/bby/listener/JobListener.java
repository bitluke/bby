package com.ut.bby.listener;

import com.ut.bby.dbmodel.Review;
import com.ut.bby.exception.BBYException;
import com.ut.bby.facade.ProductFacade;
import com.ut.bby.facade.ReviewFacade;
import com.ut.bby.util.bby.Client;
import com.ut.bby.util.xml.Converter;
import com.ut.bby.xmlmodel.Reviews;
import org.jboss.solder.logging.Logger;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.*;


public class JobListener implements ServletContextListener {
    @EJB
    private ReviewFacade reviewFacade;
    @EJB
    private ProductFacade productFacade;
    private Client client = new Client("59pu5s22t6uc94jx377vgrme");
    @Inject
    Logger log;
    Timer reviewtimer;
    Timer producttimer;

    public void contextDestroyed(ServletContextEvent arg0) {
        reviewtimer.cancel();
        producttimer.cancel();
    }

    public void contextInitialized(ServletContextEvent arg0) {
        startProductJob();
        startReviewJob();
    }


    private void startReviewJob() {
        reviewtimer = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                try {

                    log.info("executing Review job");

                    Long totalPages = 0l;
                    Long page = 0l;
                    int counter = 0;
                    do {
                        List<String> reviewtFilters = new ArrayList<String>();
                        reviewtFilters.add("id=*");
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("pageSize", "100");
                        params.put("page", String.valueOf(++page));
                        Reviews reviews = client.getReviews(reviewtFilters, params);
                        if(counter == 0){
                            totalPages = reviews.getTotalPages();
                        }
                        ++counter;
                        page = reviews.getCurrentPage();
                        if (reviews != null && reviews.getReviewList() != null) {
                            List<Review> rs = Converter.convertXmlReviewsToDbReviews(reviews.getReviewList());
                            reviewFacade.batchReviewCreate(rs);
                        }
                    } while (page < totalPages);

                } catch (BBYException e) {
                    log.error("", e);
                }
            }
        };
        reviewtimer.schedule(tt, Calendar.getInstance().getTime(), 24 * 60 * 60 * 1000);
        //reviewtimer.schedule(tt, Calendar.getInstance().getTime(), 30 * 1000);

        System.out.println("done adding Review job");
    }

    private void startProductJob() {
        producttimer = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                try {
                    Long totalPages = 0l;
                    Long page = 0l;
                    int counter = 0;
                    do {
                        List<String> productFilters = new ArrayList<String>();
                        productFilters.add("sku=*");
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("pageSize", "100");
                        params.put("page", String.valueOf(++page));
                        com.ut.bby.xmlmodel.Products xmlProducts = client.getProducts(productFilters, params);
                        if(counter == 0){
                            totalPages = xmlProducts.getTotalPages();
                        }
                        ++counter;
                        page = xmlProducts.getCurrentPage();
                        if (xmlProducts != null && xmlProducts.getProducts() != null) {
                            List<com.ut.bby.dbmodel.Product> ps = Converter.convertXmlProductsToDbProducts(xmlProducts.getProducts());
                            productFacade.batchProductCreate(ps);
                        }
                    } while (page < totalPages);

                } catch (BBYException e) {
                    log.error("", e);
                }
            }
        };
        producttimer.schedule(tt,Calendar.getInstance().getTime(),24 * 60 * 60 * 1000);
        //producttimer.schedule(tt, Calendar.getInstance().getTime(), 60 * 1000);
        log.info("done adding Product job");
    }


}