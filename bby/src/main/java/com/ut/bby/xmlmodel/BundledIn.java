package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement
public class BundledIn {
    private Long sku;

    public Long getSku() {
        return sku;
    }

    public void setSku(Long sku) {
        this.sku = sku;
    }
}
