package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement
public class Accessories {
    private List<Long> sku;

    public List<Long> getSku() {

        return sku;
    }

    public void setSku(List<Long> sku) {
        this.sku = sku;
    }
}
