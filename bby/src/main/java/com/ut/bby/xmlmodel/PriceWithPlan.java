package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class PriceWithPlan {
    private Float newTwoYearPlan;
    private Float upgradeTwoYearPlan;

    public Float getNewTwoYearPlan() {
        return newTwoYearPlan;
    }

    public void setNewTwoYearPlan(Float newTwoYearPlan) {
        this.newTwoYearPlan = newTwoYearPlan;
    }

    public Float getUpgradeTwoYearPlan() {
        return upgradeTwoYearPlan;
    }

    public void setUpgradeTwoYearPlan(Float upgradeTwoYearPlan) {
        this.upgradeTwoYearPlan = upgradeTwoYearPlan;
    }
}
