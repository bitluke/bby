package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Reviewer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
