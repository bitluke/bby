package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement
public class RelatedProducts {
  private List<Long> sku;

    @XmlElement(name = "sku")
    public List<Long> getSku() {
        return sku;
    }

    public void setSku(List<Long> sku) {
        this.sku = sku;
    }
}
