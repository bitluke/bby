package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;


@XmlRootElement
public class Product {

    //product Reference Information
    private Boolean active;
    private Date activeUpdateDate;
    private Long bestBuyItemId;
    private Integer bestSellingRank;
    private BundledIn bundledIn;
    private CategoryPath categoryPath;


    private String clazz;
    private Integer classId;
    private String collection;
    private String department;
    private Integer departmentId;
    private Date itemUpdateDate;
    private String listingId;


    private Boolean marketplace;
    private Members members;
    private String modelNumber;
    private String name;


    private Boolean neew;
    private Long productId;
    private Integer salesRankLongTerm;
    private Integer salesRankMediumTerm;
    private Integer salesRankShortTerm;
    private Boolean secondaryMarket;
    private String sellerId;
    private Long sku;

    private String source;
    private Date startDate;
    private String subclass;
    private Integer subclassId;
    private String type;
    private String upc;


    //Product Information
    private Accessories accessories;
    private String color;
    private String productCondition;
    private Float customerReviewAverage;
    private Integer customerReviewCount;
    private String depth;
    private String description;
    private Details details;
    private Boolean digital;
    private Features features;
    private String format;
    private FrequentlyPurchasedWith frequentlyPurchasedWith;
    private String height;
    private IncludedItemList includedItemList;
    private String longDescription;
    private String longDescriptionHtml;
    private String manufacturer;
    private Boolean preowned;
    private String productTemplate;
    private Integer quantityLimit;
    private RelatedProducts relatedProducts;
    private Date releaseDate;
    private String shortDescription;
    private String shortDescriptionHtml;
    private String warrantyLabor;
    private String warrantyParts;
    private String weight;
    private String width;


    //Price
    private Float dollarSavings;
    private Offers offers;
    private Boolean onSale;
    private String priceRestriction;
    private Date priceUpdateDate;
    private PriceWithPlan priceWithPlan;
    private Float regularPrice;
    private Float salePrice;


    //delivery
    private Boolean freeShipping;
    private Boolean friendsAndFamilyPickup;
    private Boolean homeDelivery;
    private Boolean inStoreAvailability;
    private String inStoreAvailabilityText;
    private String inStoreAvailabilityTextHtml;
    private Date inStoreAvailabilityUpdateDate;
    private Boolean inStorePickup;
    private Boolean onlineAvailability;
    private String onlineAvailabilityText;
    private String onlineAvailabilityTextHtml;
    private Date onlineAvailabilityUpdateDate;
    private String orderable;
    private Shipping shipping;
    private Float shippingCost;
    private String shippingWeight;
    private Boolean specialOrder;


    //Images
    private String accessoriesImage;
    private String alternateViewsImage;
    private String angleImage;
    private String backViewImage;
    private String energyGuideImage;
    private String image;
    private String largeFrontImage;
    private String largeImage;
    private String leftViewImage;
    private String mediumImage;
    private String remoteControlImage;
    private String rightViewImage;
    private String spin360Url;
    private String thumbnailImage;
    private String topViewImage;


    //Links
    private String addToCartUrl;
    private String cjAffiliateAddToCartUrl;
    private String cjAffiliateUrl;
    private String mobileUrl;
    private String url;


    //Review
    private List<Review> reviewList;



    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getActiveUpdateDate() {
        return activeUpdateDate;
    }

    public void setActiveUpdateDate(Date activeUpdateDate) {
        this.activeUpdateDate = activeUpdateDate;
    }

    public Long getBestBuyItemId() {
        return bestBuyItemId;
    }

    public void setBestBuyItemId(Long bestBuyItemId) {
        this.bestBuyItemId = bestBuyItemId;
    }

    public Integer getBestSellingRank() {
        return bestSellingRank;
    }

    public void setBestSellingRank(Integer bestSellingRank) {
        this.bestSellingRank = bestSellingRank;
    }

    public BundledIn getBundledIn() {
        return bundledIn;
    }

    public void setBundledIn(BundledIn bundledIn) {
        this.bundledIn = bundledIn;
    }

    public CategoryPath getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(CategoryPath categoryPath) {
        this.categoryPath = categoryPath;
    }

    @XmlElement(name = "class")
    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Date getItemUpdateDate() {
        return itemUpdateDate;
    }

    public void setItemUpdateDate(Date itemUpdateDate) {
        this.itemUpdateDate = itemUpdateDate;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public Boolean getMarketplace() {
        return marketplace;
    }

    public void setMarketplace(Boolean marketplace) {
        this.marketplace = marketplace;
    }

    public Members getMembers() {
        return members;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "new")
    public Boolean getNeew() {
        return neew;
    }

    public void setNeew(Boolean neew) {
        this.neew = neew;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getSalesRankLongTerm() {
        return salesRankLongTerm;
    }

    public void setSalesRankLongTerm(Integer salesRankLongTerm) {
        this.salesRankLongTerm = salesRankLongTerm;
    }

    public Integer getSalesRankMediumTerm() {
        return salesRankMediumTerm;
    }

    public void setSalesRankMediumTerm(Integer salesRankMediumTerm) {
        this.salesRankMediumTerm = salesRankMediumTerm;
    }

    public Integer getSalesRankShortTerm() {
        return salesRankShortTerm;
    }

    public void setSalesRankShortTerm(Integer salesRankShortTerm) {
        this.salesRankShortTerm = salesRankShortTerm;
    }

    public Boolean getSecondaryMarket() {
        return secondaryMarket;
    }

    public void setSecondaryMarket(Boolean secondaryMarket) {
        this.secondaryMarket = secondaryMarket;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Long getSku() {
        return sku;
    }

    public void setSku(Long sku) {
        this.sku = sku;
    }


    @XmlElement(defaultValue = "bestbuy")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getSubclass() {
        return subclass;
    }

    public void setSubclass(String subclass) {
        this.subclass = subclass;
    }

    public Integer getSubclassId() {
        return subclassId;
    }

    public void setSubclassId(Integer subclassId) {
        this.subclassId = subclassId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public Accessories getAccessories() {
        return accessories;
    }

    public void setAccessories(Accessories accessories) {
        this.accessories = accessories;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @XmlElement(name = "condition")
    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public Float getCustomerReviewAverage() {
        return customerReviewAverage;
    }

    public void setCustomerReviewAverage(Float customerReviewAverage) {
        this.customerReviewAverage = customerReviewAverage;
    }

    public Integer getCustomerReviewCount() {
        return customerReviewCount;
    }

    public void setCustomerReviewCount(Integer customerReviewCount) {
        this.customerReviewCount = customerReviewCount;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public Boolean getDigital() {
        return digital;
    }

    public void setDigital(Boolean digital) {
        this.digital = digital;
    }

    public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public FrequentlyPurchasedWith getFrequentlyPurchasedWith() {
        return frequentlyPurchasedWith;
    }

    public void setFrequentlyPurchasedWith(FrequentlyPurchasedWith frequentlyPurchasedWith) {
        this.frequentlyPurchasedWith = frequentlyPurchasedWith;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public IncludedItemList getIncludedItemList() {
        return includedItemList;
    }

    public void setIncludedItemList(IncludedItemList includedItemList) {
        this.includedItemList = includedItemList;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getLongDescriptionHtml() {
        return longDescriptionHtml;
    }

    public void setLongDescriptionHtml(String longDescriptionHtml) {
        this.longDescriptionHtml = longDescriptionHtml;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Boolean getPreowned() {
        return preowned;
    }

    public void setPreowned(Boolean preowned) {
        this.preowned = preowned;
    }

    public String getProductTemplate() {
        return productTemplate;
    }

    public void setProductTemplate(String productTemplate) {
        this.productTemplate = productTemplate;
    }

    public Integer getQuantityLimit() {
        return quantityLimit;
    }

    public void setQuantityLimit(Integer quantityLimit) {
        this.quantityLimit = quantityLimit;
    }

    public RelatedProducts getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(RelatedProducts relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescriptionHtml() {
        return shortDescriptionHtml;
    }

    public void setShortDescriptionHtml(String shortDescriptionHtml) {
        this.shortDescriptionHtml = shortDescriptionHtml;
    }

    public String getWarrantyLabor() {
        return warrantyLabor;
    }

    public void setWarrantyLabor(String warrantyLabor) {
        this.warrantyLabor = warrantyLabor;
    }

    public String getWarrantyParts() {
        return warrantyParts;
    }

    public void setWarrantyParts(String warrantyParts) {
        this.warrantyParts = warrantyParts;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public Float getDollarSavings() {
        return dollarSavings;
    }

    public void setDollarSavings(Float dollarSavings) {
        this.dollarSavings = dollarSavings;
    }

    public Offers getOffers() {
        return offers;
    }

    public void setOffers(Offers offers) {
        this.offers = offers;
    }

    public Boolean getOnSale() {
        return onSale;
    }

    public void setOnSale(Boolean onSale) {
        this.onSale = onSale;
    }

    public String getPriceRestriction() {
        return priceRestriction;
    }

    public void setPriceRestriction(String priceRestriction) {
        this.priceRestriction = priceRestriction;
    }

    public Date getPriceUpdateDate() {
        return priceUpdateDate;
    }

    public void setPriceUpdateDate(Date priceUpdateDate) {
        this.priceUpdateDate = priceUpdateDate;
    }

    public PriceWithPlan getPriceWithPlan() {
        return priceWithPlan;
    }

    public void setPriceWithPlan(PriceWithPlan priceWithPlan) {
        this.priceWithPlan = priceWithPlan;
    }

    public Float getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(Float regularPrice) {
        this.regularPrice = regularPrice;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }

    public Boolean getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(Boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public Boolean getFriendsAndFamilyPickup() {
        return friendsAndFamilyPickup;
    }

    public void setFriendsAndFamilyPickup(Boolean friendsAndFamilyPickup) {
        this.friendsAndFamilyPickup = friendsAndFamilyPickup;
    }

    public Boolean getHomeDelivery() {
        return homeDelivery;
    }

    public void setHomeDelivery(Boolean homeDelivery) {
        this.homeDelivery = homeDelivery;
    }

    public Boolean getInStoreAvailability() {
        return inStoreAvailability;
    }

    public void setInStoreAvailability(Boolean inStoreAvailability) {
        this.inStoreAvailability = inStoreAvailability;
    }

    public String getInStoreAvailabilityText() {
        return inStoreAvailabilityText;
    }

    public void setInStoreAvailabilityText(String inStoreAvailabilityText) {
        this.inStoreAvailabilityText = inStoreAvailabilityText;
    }

    public String getInStoreAvailabilityTextHtml() {
        return inStoreAvailabilityTextHtml;
    }

    public void setInStoreAvailabilityTextHtml(String inStoreAvailabilityTextHtml) {
        this.inStoreAvailabilityTextHtml = inStoreAvailabilityTextHtml;
    }

    public Date getInStoreAvailabilityUpdateDate() {
        return inStoreAvailabilityUpdateDate;
    }

    public void setInStoreAvailabilityUpdateDate(Date inStoreAvailabilityUpdateDate) {
        this.inStoreAvailabilityUpdateDate = inStoreAvailabilityUpdateDate;
    }

    public Boolean getInStorePickup() {
        return inStorePickup;
    }

    public void setInStorePickup(Boolean inStorePickup) {
        this.inStorePickup = inStorePickup;
    }

    public Boolean getOnlineAvailability() {
        return onlineAvailability;
    }

    public void setOnlineAvailability(Boolean onlineAvailability) {
        this.onlineAvailability = onlineAvailability;
    }

    public String getOnlineAvailabilityText() {
        return onlineAvailabilityText;
    }

    public void setOnlineAvailabilityText(String onlineAvailabilityText) {
        this.onlineAvailabilityText = onlineAvailabilityText;
    }

    public String getOnlineAvailabilityTextHtml() {
        return onlineAvailabilityTextHtml;
    }

    public void setOnlineAvailabilityTextHtml(String onlineAvailabilityTextHtml) {
        this.onlineAvailabilityTextHtml = onlineAvailabilityTextHtml;
    }

    public Date getOnlineAvailabilityUpdateDate() {
        return onlineAvailabilityUpdateDate;
    }

    public void setOnlineAvailabilityUpdateDate(Date onlineAvailabilityUpdateDate) {
        this.onlineAvailabilityUpdateDate = onlineAvailabilityUpdateDate;
    }

    public String getOrderable() {
        return orderable;
    }

    public void setOrderable(String orderable) {
        this.orderable = orderable;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public Float getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(Float shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingWeight() {
        return shippingWeight;
    }

    public void setShippingWeight(String shippingWeight) {
        this.shippingWeight = shippingWeight;
    }

    public Boolean getSpecialOrder() {
        return specialOrder;
    }

    public void setSpecialOrder(Boolean specialOrder) {
        this.specialOrder = specialOrder;
    }

    public String getAccessoriesImage() {
        return accessoriesImage;
    }

    public void setAccessoriesImage(String accessoriesImage) {
        this.accessoriesImage = accessoriesImage;
    }

    public String getAlternateViewsImage() {
        return alternateViewsImage;
    }

    public void setAlternateViewsImage(String alternateViewsImage) {
        this.alternateViewsImage = alternateViewsImage;
    }

    public String getAngleImage() {
        return angleImage;
    }

    public void setAngleImage(String angleImage) {
        this.angleImage = angleImage;
    }

    public String getBackViewImage() {
        return backViewImage;
    }

    public void setBackViewImage(String backViewImage) {
        this.backViewImage = backViewImage;
    }

    public String getEnergyGuideImage() {
        return energyGuideImage;
    }

    public void setEnergyGuideImage(String energyGuideImage) {
        this.energyGuideImage = energyGuideImage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLargeFrontImage() {
        return largeFrontImage;
    }

    public void setLargeFrontImage(String largeFrontImage) {
        this.largeFrontImage = largeFrontImage;
    }

    public String getLargeImage() {
        return largeImage;
    }

    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }

    public String getLeftViewImage() {
        return leftViewImage;
    }

    public void setLeftViewImage(String leftViewImage) {
        this.leftViewImage = leftViewImage;
    }

    public String getMediumImage() {
        return mediumImage;
    }

    public void setMediumImage(String mediumImage) {
        this.mediumImage = mediumImage;
    }

    public String getRemoteControlImage() {
        return remoteControlImage;
    }

    public void setRemoteControlImage(String remoteControlImage) {
        this.remoteControlImage = remoteControlImage;
    }

    public String getRightViewImage() {
        return rightViewImage;
    }

    public void setRightViewImage(String rightViewImage) {
        this.rightViewImage = rightViewImage;
    }

    public String getSpin360Url() {
        return spin360Url;
    }

    public void setSpin360Url(String spin360Url) {
        this.spin360Url = spin360Url;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getTopViewImage() {
        return topViewImage;
    }

    public void setTopViewImage(String topViewImage) {
        this.topViewImage = topViewImage;
    }

    public String getAddToCartUrl() {
        return addToCartUrl;
    }

    public void setAddToCartUrl(String addToCartUrl) {
        this.addToCartUrl = addToCartUrl;
    }

    public String getCjAffiliateAddToCartUrl() {
        return cjAffiliateAddToCartUrl;
    }

    public void setCjAffiliateAddToCartUrl(String cjAffiliateAddToCartUrl) {
        this.cjAffiliateAddToCartUrl = cjAffiliateAddToCartUrl;
    }

    public String getCjAffiliateUrl() {
        return cjAffiliateUrl;
    }

    public void setCjAffiliateUrl(String cjAffiliateUrl) {
        this.cjAffiliateUrl = cjAffiliateUrl;
    }

    public String getMobileUrl() {
        return mobileUrl;
    }

    public void setMobileUrl(String mobileUrl) {
        this.mobileUrl = mobileUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}

