package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;



@XmlRootElement
public class IncludedItemList {
    private List<String> includedItem;


    public List<String> getIncludedItem() {
        return includedItem;
    }

    public void setIncludedItem(List<String> includedItem) {
        this.includedItem = includedItem;
    }
}
