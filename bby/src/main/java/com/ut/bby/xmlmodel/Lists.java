package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Lists {
    private String endDate;
    private String listId;
    private String startDate;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
}
