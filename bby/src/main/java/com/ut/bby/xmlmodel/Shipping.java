package com.ut.bby.xmlmodel;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Shipping {
    private Float ground;
    private Float nextDay;
    private Float secondDay;
    private Float vendorDelivery;

    public Float getGround() {
        return ground;
    }

    public void setGround(Float ground) {
        this.ground = ground;
    }

    public Float getNextDay() {
        return nextDay;
    }

    public void setNextDay(Float nextDay) {
        this.nextDay = nextDay;
    }

    public Float getSecondDay() {
        return secondDay;
    }

    public void setSecondDay(Float secondDay) {
        this.secondDay = secondDay;
    }

    public Float getVendorDelivery() {
        return vendorDelivery;
    }

    public void setVendorDelivery(Float vendorDelivery) {
        this.vendorDelivery = vendorDelivery;
    }
}
