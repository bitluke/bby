package com.ut.bby.util.xml;

import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.List;


public class Parser<T> {
    private List<T> entityList;
    private Class<T> entityClass;
    private T entity;
    private JAXBContext context;

    public Parser(Class clazz) {
        entityClass = clazz;
    }


    public T unMarshalFile(String filePath) {
        try {
            context = JAXBContext.newInstance(entityClass);
            //Reader
            Unmarshaller um = context.createUnmarshaller();
            entity = (T) um.unmarshal(new FileReader(filePath));
        } catch (JAXBException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return entity;
    }

    public T unMarshalResponse(InputStream inputStream) {
        try {
            context = JAXBContext.newInstance(entityClass);
            //Reader
            Unmarshaller um = context.createUnmarshaller();
            entity = (T) um.unmarshal(new InputSource(inputStream));
        } catch (JAXBException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return entity;
    }






}
