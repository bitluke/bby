package com.ut.bby.util.xml;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ParserUtil {
    /**
     * Delimiter used to join multiple resource filters
     */
    public final static String DELIMITER_FILTER = "&";

    /**
     *  String delimiter used to separate query string parameters
     */
    public final static String DELIMITER_QUERY = "&";

    /**
     * Builds a resource URI filter string (e.g. "(name=foo&bar>=10)") from a list of filters.
     *
     * @param  filters List of individual filters to be joined into this filter string
     * @return Resource URI filter string
     */
    public static String buildFilterString(List<String> filters) {
        StringBuffer filter = new StringBuffer();
        if(null != filters) {
            for(Iterator<String> iter = filters.iterator(); iter.hasNext();) {
                filter.append(iter.next());
                if(iter.hasNext()) {
                    filter.append(DELIMITER_FILTER);
                }
            }
        }
        return 0 < filter.length() ? "(" + filter + ")" : "";
    }

    /**
     * Builds a query string from a map of name/value pairs.
     *
     * @param  params Query parameters
     * @return Formatted query string
     */
    public static String buildQueryString(Map<String, String> params) {
        StringBuffer query = new StringBuffer();
        if(null != params) {
            List<String> fragments = new LinkedList<String>();
            for(Iterator<String> iter = params.keySet().iterator(); iter.hasNext();) {
                String key = iter.next();
                String val = params.get(key);
                if(null != val) {
                    fragments.add(key + "=" + val);
                }
            }
            for(Iterator<String> iter = fragments.iterator(); iter.hasNext();) {
                query.append(iter.next());
                if(iter.hasNext()) {
                    query.append(DELIMITER_QUERY);
                }
            }
        }
        return (0 < query.length()) ? "?" + query.toString() : "";
    }

}
