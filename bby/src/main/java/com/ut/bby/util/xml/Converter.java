package com.ut.bby.util.xml;

import com.ut.bby.dbmodel.Category;
import com.ut.bby.dbmodel.Offer;
import com.ut.bby.dbmodel.Product;
import com.ut.bby.dbmodel.Review;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static Review convertXmlReviewToDbReview(com.ut.bby.xmlmodel.Review xmlReview) {
        Review convertedReview = new Review();
        convertedReview.setAboutMeCustomerType(xmlReview.getAboutMe());
        convertedReview.setComment(xmlReview.getComment());
        convertedReview.setRating(Float.valueOf(xmlReview.getRating()));
        convertedReview.setReviewerName(xmlReview.getReviewer() != null ? xmlReview.getReviewer().getName() : "");
        convertedReview.setReviewId(Long.valueOf(xmlReview.getReviewId()));
        convertedReview.setSku(Long.valueOf(xmlReview.getSku()));
        convertedReview.setSubmissionTime(xmlReview.getSubmissionTime());
        convertedReview.setTitle(xmlReview.getTitle());
        return convertedReview;
    }

    public static List<Product> convertXmlProductsToDbProducts(List<com.ut.bby.xmlmodel.Product> xmlProducts) {
        List<Product> converteredProducts = new ArrayList<Product>();
        for (com.ut.bby.xmlmodel.Product p : xmlProducts) {
            converteredProducts.add(convertXmlProductToDbProduct(p));
        }
        return converteredProducts;
    }

    public static List<Review> convertXmlReviewsToDbReviews(List<com.ut.bby.xmlmodel.Review> reviews) {
        List<Review> converteredReviews = new ArrayList<Review>();
        for (com.ut.bby.xmlmodel.Review r : reviews) {
            converteredReviews.add(convertXmlReviewToDbReview(r));
        }
        return converteredReviews;
    }


    public static Product convertXmlProductToDbProduct(com.ut.bby.xmlmodel.Product xmlProduct) {
        Product converteredProduct = new Product();
        converteredProduct.setSku(xmlProduct.getSku());
        converteredProduct.setProductId(xmlProduct.getProductId());
        converteredProduct.setName(xmlProduct.getName());
        converteredProduct.setType(xmlProduct.getType());
        converteredProduct.setStartDate(xmlProduct.getStartDate());
        converteredProduct.setNeew(xmlProduct.getNeew());
        converteredProduct.setActive(xmlProduct.getActive());
        converteredProduct.setActiveUpdateDate(xmlProduct.getActiveUpdateDate());
        converteredProduct.setRegularPrice(xmlProduct.getRegularPrice());
        converteredProduct.setSalePrice(xmlProduct.getSalePrice());
        converteredProduct.setPriceUpdateDate(xmlProduct.getPriceUpdateDate());
        converteredProduct.setUrl(xmlProduct.getUrl());
        converteredProduct.setAddToCartUrl(xmlProduct.getAddToCartUrl());
        converteredProduct.setCjAffiliateUrl(xmlProduct.getCjAffiliateUrl());
        converteredProduct.setUpc(xmlProduct.getUpc());

        List<Category> categories = new ArrayList<Category>();
        if (xmlProduct.getCategoryPath() != null && xmlProduct.getCategoryPath().getCategoryList() != null) {
            for (com.ut.bby.xmlmodel.Category c : xmlProduct.getCategoryPath().getCategoryList()) {
                Category newCat = new Category();
                newCat.setName(c.getName());
                newCat.setCategoryId(c.getId());
                categories.add(newCat);
            }
        }


        converteredProduct.setCategoryList(categories);

        converteredProduct.setCustomerReviewCount(xmlProduct.getCustomerReviewCount());
        converteredProduct.setCustomerReviewAverage(xmlProduct.getCustomerReviewAverage());
        converteredProduct.setFormat(xmlProduct.getFormat());
        converteredProduct.setFreeShipping(xmlProduct.getFreeShipping());
        converteredProduct.setInStoreAvailability(xmlProduct.getInStoreAvailability());
        converteredProduct.setInStoreAvailabilityText(xmlProduct.getInStoreAvailabilityText());
        converteredProduct.setInStoreAvailabilityUpdateDate(xmlProduct.getInStoreAvailabilityUpdateDate());
        converteredProduct.setItemUpdateDate(xmlProduct.getItemUpdateDate());
        converteredProduct.setOnlineAvailability(xmlProduct.getOnlineAvailability());
        converteredProduct.setOnlineAvailabilityText(xmlProduct.getOnlineAvailabilityText());
        converteredProduct.setOnlineAvailabilityUpdateDate(xmlProduct.getOnlineAvailabilityUpdateDate());
        converteredProduct.setReleaseDate(xmlProduct.getReleaseDate());
        converteredProduct.setShippingCost(xmlProduct.getShippingCost());
        converteredProduct.setSpecialOrder(xmlProduct.getSpecialOrder());
        converteredProduct.setShortDescription(xmlProduct.getShortDescription());
        converteredProduct.setClazz(xmlProduct.getClazz());
        converteredProduct.setClassId(xmlProduct.getClassId());
        converteredProduct.setSubclass(xmlProduct.getSubclass());
        converteredProduct.setSubclassId(xmlProduct.getSubclassId());
        converteredProduct.setDepartment(xmlProduct.getDepartment());
        converteredProduct.setDepartmentId(xmlProduct.getDepartmentId());
        converteredProduct.setDescription(xmlProduct.getDescription());
        converteredProduct.setManufacturer(xmlProduct.getManufacturer());
        converteredProduct.setModelNumber(xmlProduct.getModelNumber());
        converteredProduct.setImage(xmlProduct.getImage());
        converteredProduct.setLargeImage(xmlProduct.getLargeImage());
        converteredProduct.setLargeFrontImage(xmlProduct.getLargeFrontImage());
        converteredProduct.setMediumImage(xmlProduct.getMediumImage());
        converteredProduct.setThumbnailImage(xmlProduct.getThumbnailImage());
        converteredProduct.setAlternateViewsImage(xmlProduct.getAlternateViewsImage());
        converteredProduct.setAngleImage(xmlProduct.getAngleImage());
        converteredProduct.setBackViewImage(xmlProduct.getBackViewImage());
        converteredProduct.setEnergyGuideImage(xmlProduct.getEnergyGuideImage());
        converteredProduct.setLeftViewImage(xmlProduct.getLeftViewImage());
        converteredProduct.setAccessoriesImage(xmlProduct.getAccessoriesImage());
        converteredProduct.setRemoteControlImage(xmlProduct.getRemoteControlImage());
        converteredProduct.setRightViewImage(xmlProduct.getRightViewImage());
        converteredProduct.setTopViewImage(xmlProduct.getTopViewImage());
        converteredProduct.setColor(xmlProduct.getColor());
        converteredProduct.setDepth(xmlProduct.getDepth());
        converteredProduct.setDollarSavings(xmlProduct.getDollarSavings());
        converteredProduct.setHeight(xmlProduct.getHeight());
        converteredProduct.setOrderable(xmlProduct.getOrderable());
        converteredProduct.setWeight(xmlProduct.getWeight());
        converteredProduct.setShippingWeight(xmlProduct.getShippingWeight());
        converteredProduct.setWidth(xmlProduct.getWidth());
        converteredProduct.setWarrantyLabor(xmlProduct.getWarrantyLabor());
        converteredProduct.setWarrantyParts(xmlProduct.getWarrantyParts());
        converteredProduct.setLongDescription(xmlProduct.getLongDescription());
        converteredProduct.setLongDescription(xmlProduct.getLongDescription());
        converteredProduct.setDetailsName(xmlProduct.getDetails() != null ? xmlProduct.getDetails().getName() : null);
        converteredProduct.setDetailsValue(xmlProduct.getDetails() != null ? xmlProduct.getDetails().getValue() : null);
        converteredProduct.setFeature(xmlProduct.getFeatures() != null ? xmlProduct.getFeatures().getFeature() : null);

        //
        List<Offer> convertedOffer = new ArrayList<Offer>();

        if (xmlProduct.getOffers() != null && xmlProduct.getOffers().getOfferList() != null) {
            for (com.ut.bby.xmlmodel.Offer of : xmlProduct.getOffers().getOfferList()) {
                Offer newOffer = new Offer();
                newOffer.setOfferId(of.getId());
                newOffer.setEndDate(of.getEndDate());
                newOffer.setHeading(of.getHeading());
                newOffer.setImageUrl(of.getImageUrl());
                newOffer.setUrl(of.getUrl());
                newOffer.setText(of.getText());
                newOffer.setType(of.getType());
                newOffer.setStartDate(of.getStartDate());
                //add
                convertedOffer.add(newOffer);
            }
        }


        converteredProduct.setOfferList(convertedOffer);
        converteredProduct.setRelatedProductsSku(xmlProduct.getRelatedProducts() != null ? xmlProduct.getRelatedProducts().getSku() : null);
        converteredProduct.setSource(xmlProduct.getSource());
        return converteredProduct;
    }
}
