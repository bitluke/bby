package com.ut.bby.util.manager;

import com.ut.bby.util.qualifier.PersistenceQualifier;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EntityManagerProducer {

    @Produces
    @PersistenceQualifier
    @PersistenceContext(unitName = "com_ut_bby_PU")
    EntityManager em;


}
