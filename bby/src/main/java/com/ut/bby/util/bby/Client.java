package com.ut.bby.util.bby;

import com.ut.bby.exception.BBYException;
import com.ut.bby.util.xml.Parser;
import com.ut.bby.util.xml.ParserUtil;
import com.ut.bby.xmlmodel.Product;
import com.ut.bby.xmlmodel.Products;
import com.ut.bby.xmlmodel.Review;
import com.ut.bby.xmlmodel.Reviews;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Client {

    /**
     * Entry point for all API calls
     */
    public static final String ENTRY_POINT = "http://api.remix.bestbuy.com/v1/";


    /**
     * Resource URI
     */
    public static final String
            PATH_STORES = "stores",
            PATH_STORE = "stores/%s.xml",
            PATH_PRODUCTS = "products",
            PATH_PRODUCT = "products/%s.xml",
            PATH_REVIEW = "reviews",
            PATH_REVIEWS = "reviews/%s.xml",
            PATH_PRODUCT_ARCHIVE = "products.xml.zip",
            PATH_REVIEW_ARCHIVE = "reviews.xml.zip",
            PATH_CATEGORY_ARCHIVE = "products/%s.xml",

    PATH_STORE_ARCHIVE = "products/%s.xml";


    /**
     * Default User-Agent string sent with each API request
     */
    public static final String USER_AGENT = "com.mattwilliamsnyc.service.remix Java client";

    /**
     * Client library version string included with User-Agent header
     */
    public static final String VERSION = "1.0.0";
    public static final String PAGE_SIZE = "pageSize";
    public static final String PAGE = "page";

    /**
     * Developer key used to access to the Remix API
     */
    private String apiKey;

    /**
     * Request headers to be sent with an API call
     */
    private Map<String, String> headers = new HashMap<String, String>();


    public Client(String apiKey) {
        this.apiKey = apiKey;
    }


    /**
     * Makes an API call to the "product" resource, targeted by SKU #.
     *
     * @param sku SKU # used to identify the product resource being retrieved
     * @return API response
     * @throws com.ut.bby.exception.BBYException
     *
     */
    public Product getProduct(int sku) throws BBYException {
        return getProduct(String.valueOf(sku), null);
    }


    /**
     * Makes an API call to the "review" resource, targeted by SKU #.
     *
     * @param id SKU # used to identify the product resource being retrieved
     * @return Review
     * @throws com.ut.bby.exception.BBYException
     *
     */
    public Review getReview(int id) throws BBYException {
        return getReview(String.valueOf(id), null);
    }


    /**
     * Makes an API call to the "products" collection resource.
     *
     * @return Products
     * @throws BBYException
     */
    public Products getProducts() throws BBYException {
        return getProducts(null, null);
    }

    /**
     * Makes an API call to the "products" collection resource.
     *
     * @return Reviews
     * @throws BBYException
     */
    public Reviews getReviews() throws BBYException {
        return getReviews(null, null);
    }

    /**
     * Makes an API call to the "products" collection resource.
     *
     * @param filters List of filters to be applied to the products collection
     * @return Products
     * @throws BBYException
     */
    public Products getProducts(List<String> filters) throws BBYException {
        return getProducts(filters, null);
    }


    /**
     * Makes an API call to the "products" collection resource.
     *
     * @param filters List of filters to be applied to the products collection
     * @return Reviews
     * @throws BBYException
     */
    public Reviews getReviews(List<String> filters) throws BBYException {
        return getReviews(filters, null);
    }


    /**
     * Makes an API call to the "products" collection resource.
     *
     * @param filters List of filters to be applied to the products collection
     * @param params  Query string parameters
     * @return API response
     * @throws BBYException
     */
    public Products getProducts(List<String> filters, Map<String, String> params) throws BBYException {
        String uri = PATH_PRODUCTS + ParserUtil.buildFilterString(filters);
        HttpURLConnection connection = query("GET", uri, params);
        try {
            Parser parser = new Parser(Products.class);
            return (Products) parser.unMarshalResponse(connection.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


    /**
     * Makes an API call to the "products" collection resource.
     *
     * @return API response
     * @throws BBYException
     */
    public Products getProductsFromZipContent(InputStream inputStream) throws BBYException {
        Parser parser = new Parser(Products.class);
        return (Products) parser.unMarshalResponse(inputStream);
    }

    /**
     * Makes an API call to the "products" collection resource.
     *
     * @return API response
     * @throws BBYException
     */
    public Reviews getReviewsFromZipContent(InputStream inputStream) throws BBYException {
        Parser parser = new Parser(Reviews.class);
        return (Reviews) parser.unMarshalResponse(inputStream);
    }


    /**
     * Makes an API call to the "products" collection resource.
     *
     * @param filters List of filters to be applied to the products collection
     * @param params  Query string parameters
     * @return API response
     * @throws BBYException
     */
    public Reviews getReviews(List<String> filters, Map<String, String> params) throws BBYException {
        String uri = PATH_REVIEW + ParserUtil.buildFilterString(filters);
        HttpURLConnection connection = query("GET", uri, params);
        try {
            Parser parser = new Parser(Reviews.class);
            return (Reviews) parser.unMarshalResponse(connection.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;

    }


    /**
     * Makes an Local call to the "Reviews" collection resource.
     *
     * @param localPath path to xml file
     * @return API response
     * @throws BBYException
     */
    public Reviews getReviewsLocally(String localPath) throws BBYException {
        Parser parser = new Parser(Reviews.class);
        return (Reviews) parser.unMarshalFile(localPath);
    }


    /**
     * Makes an Local call to the "Products" collection resource.
     *
     * @param localPath path to xml file
     * @return API response
     * @throws BBYException
     */
    public Products getProductsLocally(String localPath) throws BBYException {
        Parser parser = new Parser(Products.class);
        return (Products) parser.unMarshalFile(localPath);
    }


    /**
     * Makes an API call to the "product" resource, targeted by SKU #.
     *
     * @param sku    SKU # used to identify the product resource being retrieved
     * @param params Query string parameters
     * @return API response
     * @throws BBYException
     */
    public Product getProduct(String sku, Map<String, String> params) throws BBYException {
        String uri = new Formatter().format(PATH_PRODUCT, sku).toString();
        HttpURLConnection connection = query("GET", uri, params);
        try {
            Parser parser = new Parser(Product.class);
            return (Product) parser.unMarshalResponse(connection.getInputStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Makes an API call to the "product" resource, targeted by SKU #.
     *
     * @param id     SKU # used to identify the product resource being retrieved
     * @param params Query string parameters
     * @return API response
     * @throws BBYException
     */
    public Review getReview(String id, Map<String, String> params) throws BBYException {
        String uri = new Formatter().format(PATH_REVIEWS, id).toString();
        HttpURLConnection connection = query("GET", uri, params);
        try {
            Parser parser = new Parser(Review.class);
            return (Review) parser.unMarshalResponse(connection.getInputStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Makes an HTTP request and returns the {@link java.net.HttpURLConnection connection}.
     *
     * @param method HTTP method for this request
     * @param uri    Resource URI targeted by this request
     * @return HTTP Connection
     * @throws com.ut.bby.exception.BBYException
     *
     */
    private HttpURLConnection query(String method, String uri, Map<String, String> params) throws BBYException {
        HttpURLConnection connection;
        try {
            if (null == params) {
                params = new HashMap<String, String>();
            }
            params.put("apiKey", apiKey);
            URL url = new URL(ENTRY_POINT + uri + ParserUtil.buildQueryString(params));
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.addRequestProperty("User-Agent", USER_AGENT + " v" + VERSION);
            for (String key : headers.keySet()) {
                connection.addRequestProperty(key, headers.get(key));
            }
        } catch (MalformedURLException e) {
            throw new BBYException("Invalid URL", e);
        } catch (ProtocolException e) {
            throw new BBYException("Invalid request protocol", e);
        } catch (IOException e) {
            throw new BBYException("IO Error: Please try again", e);
        }
        return connection;
    }


}
