<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <sb:head/>
</head>
<body>
<s:actionerror theme="bootstrap"/>
<s:actionmessage theme="bootstrap"/>
<s:fielderror theme="bootstrap"/>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <br/>
            <br/>
            <ul class="nav nav-list">
                <li class="active"><a href="#"><i class="icon-home icon-white"></i>BBY Home</a></li>
                <li><s:a namespace="/product" action="RetrieveAllProducts"><i class="icon-book"></i>Products</s:a></li>
                <li><s:a namespace="/review" action="RetrieveAllReviews"><i class="icon-pencil"></i>Reviews</s:a></li>
                <li><s:a namespace="/upload" action="UploadPage"><i class="icon-search"></i>Upload Zip</s:a></li>
            </ul>
        </div>
        <div class="span10">
            <br/>
            <h2>Upload File</h2>
            <s:form enctype="multipart/form-data" >
                <s:file label="location of Zip file"  name="zipped"/>
                <s:submit value="Upload To Database" action="InitDb"/>
            </s:form>
        </div>
    </div>
</div>


</div>
</body>
</html>