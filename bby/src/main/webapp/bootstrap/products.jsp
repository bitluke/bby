<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <sb:head/>
</head>
<body>
<s:actionerror theme="bootstrap"/>
<s:actionmessage theme="bootstrap"/>
<s:fielderror theme="bootstrap"/>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <br/>
            <br/>
            <ul class="nav nav-list">
                <li class="active"><a href="#"><i class="icon-home icon-white"></i>BBY Home</a></li>
                <li><s:a namespace="/product" action="RetrieveAllProducts"><i class="icon-book"></i>Products</s:a></li>
                <li><s:a namespace="/review" action="RetrieveAllReviews"><i class="icon-pencil"></i>Reviews</s:a></li>
                <li><s:a namespace="/upload" action="UploadPage"><i class="icon-search"></i>Upload Zip</s:a></li>
            </ul>
        </div>
        <div class="span10">
            <br/>
            <h2>BBY| Products</h2>
            <display:table class="table table-bordered table-hover" pagesize="10" sort="external"
                           requestURI="/product/RetrieveAllProducts"
                           name="productList">
                <display:column property="displayImage"  title="Image"/>
                <display:column property="name" title="Name"/>
                <display:column property="shortDescription" title="Description"/>
                <display:column property="customerReviewAverage" title="Review Rate"/>
                <display:column url="/product/RetrieveReviews" paramId="sku" paramProperty="sku" property="customerReviewCount" title="Review Count"/>
                <display:column property="regularPrice" title="Price"/>
            </display:table>

        </div>
    </div>
</div>


</div>
</body>
</html>